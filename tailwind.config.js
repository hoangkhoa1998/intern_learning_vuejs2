/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {},
    colors: {
      orange: "#ec8946",
      whitesmoke: "#f5f5f5",
      orangev2: "orange",
      slate700: "#324055",
      slate800: "#1e293b",
      slate900: "#0f172a",
    },
  },
  plugins: [],
};
